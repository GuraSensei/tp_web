<?php

function getDinos(){
    $response = Requests::get("https://allosaurus.delahayeyourself.info/api/dinosaurs/");
    return json_decode($response->body);
}

function getDinosDetails($dinoName){
    $response = Requests::get("https://allosaurus.delahayeyourself.info/api/dinosaurs/$dinoName");
    return json_decode($response->body);
}